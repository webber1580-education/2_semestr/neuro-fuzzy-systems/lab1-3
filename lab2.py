import numpy as np  # linear algebra
import pandas as pd  # read and wrangle dataframes
import matplotlib.pyplot as plt  # visualization
import seaborn as sns  # statistical visualizations and aesthetics
from sklearn.base import TransformerMixin  # To create new classes for transformations
from sklearn.metrics import accuracy_score, confusion_matrix
from sklearn.preprocessing import (FunctionTransformer, StandardScaler)  # preprocessing
from sklearn.decomposition import PCA  # dimensionality reduction
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis as LDA
from scipy.stats import boxcox  # data transform
from sklearn.model_selection import (train_test_split, KFold, StratifiedKFold,
                                     cross_val_score, GridSearchCV,
                                     learning_curve, validation_curve, RandomizedSearchCV)  # model selection modules
from sklearn.pipeline import Pipeline  # streaming pipelines
from sklearn.base import BaseEstimator, TransformerMixin  # To create a box-cox transformation class
from collections import Counter
import warnings
# load models
from sklearn.tree import DecisionTreeClassifier
from sklearn.linear_model import LogisticRegression
from xgboost import (XGBClassifier, plot_importance)
from sklearn.svm import SVC
from sklearn.ensemble import (RandomForestClassifier, AdaBoostClassifier, ExtraTreesClassifier,
                              GradientBoostingClassifier)
from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import GaussianNB
from time import time


# Tukey's method.
def outlier_hunt(df):
    """
    Takes a dataframe df of features and returns a list of the indices
    corresponding to the observations containing more than 2 outliers.
    """
    outlier_indices = []

    # iterate over features(columns)
    for col in df.columns.tolist():
        # 1st quartile (25%)
        Q1 = np.percentile(df[col], 25)

        # 3rd quartile (75%)
        Q3 = np.percentile(df[col], 75)

        # Interquartile rrange (IQR)
        IQR = Q3 - Q1

        # outlier step
        outlier_step = 1.5 * IQR

        # Determine a list of indices of outliers for feature col
        outlier_list_col = df[(df[col] < Q1 - outlier_step) | (df[col] > Q3 + outlier_step)].index

        # append the found outlier indices for col to the list of outlier indices
        outlier_indices.extend(outlier_list_col)

    # select observations containing more than 2 outliers
    outlier_indices = Counter(outlier_indices)
    multiple_outliers = list(k for k, v in outlier_indices.items() if v > 2)

    return multiple_outliers


def get_correlation_heatmap(df):
    corr = df[features].corr()
    plt.figure(figsize=(16, 16))
    sns.heatmap(corr, cbar=True, square=True, annot=True, fmt='.2f', annot_kws={'size': 15},
                xticklabels=features, yticklabels=features, alpha=0.7, cmap='coolwarm')
    plt.savefig('graphs/correlation_heatmap.png')


def data_preparation(df):
    # clear data from outliers
    outlier_indices = outlier_hunt(df[features])
    df = df.drop(outlier_indices).reset_index(drop=True)
    return df


def svm(df, x_train, x_test, y_train, y_test):
    from sklearn.svm import SVC
    classifier_svm = SVC()
    steps = [
        ('scalar', StandardScaler()),
        ('model', SVC())
    ]
    svm_linear_pipe = Pipeline(steps)

    parameters = {'model__kernel': ['linear'],
                  'model__C': [1, 10, 100, 1000, 10000],
                  'model__random_state': [42]
                  }
    classifier_svm_linear = GridSearchCV(svm_linear_pipe, parameters, iid=False, cv=3)
    classifier_svm_linear = classifier_svm_linear.fit(x_train, y_train.ravel())

    y_pred_svm_linear_test = classifier_svm_linear.predict(x_test)
    accuracy_svm_linear_test = accuracy_score(y_test, y_pred_svm_linear_test)
    print("Test set: ", accuracy_svm_linear_test)


def random_forest(df, x_train, x_test, y_train, y_test):
    classifier_rf = RandomForestClassifier()
    steps = [
        ('scalar', StandardScaler()),
        ('model', RandomForestClassifier())
    ]
    rf_pipe = Pipeline(steps)

    parameters = {"model__n_estimators": [int(x) for x in np.linspace(start=200, stop=2000, num=10)],
                  "model__max_features": ["auto", "sqrt"],
                  "model__max_depth": np.linspace(10, 110, num=11),
                  "model__min_samples_split": [2, 5, 10],
                  "model__min_samples_leaf": [1, 2, 4],
                  "model__bootstrap": [True, False],
                  "model__criterion": ["gini"],
                  "model__random_state": [42]}

    classifier_rf = RandomizedSearchCV(estimator=rf_pipe,
                                       param_distributions=parameters,
                                       n_iter=100,
                                       cv=3,
                                       random_state=42,
                                       verbose=4,
                                       n_jobs=-1)

    classifier_rf = classifier_rf.fit(x_train, y_train.ravel())

    y_pred_rf_test = classifier_rf.predict(x_test)
    accuracy_rf_test = accuracy_score(y_test, y_pred_rf_test)
    print("Test set: ", accuracy_rf_test)

    sns.heatmap(confusion_matrix(y_test, y_pred_rf_test), annot=True, cmap='viridis', fmt='.0f')
    plt.show()


def knn_classifier(df, x_train, x_test, y_train, y_test):
    classifier_knn = KNeighborsClassifier()
    steps = [
        ('scalar', StandardScaler()),
        ('model', KNeighborsClassifier())
    ]
    knn_pipe = Pipeline(steps)

    parameters = {'model__algorithm': ['brute'],
                  'model__leaf_size': [30, 50, 70, 90, 110],
                  'model__metric': ['minkowski'],
                  'model__p': [1],
                  'model__n_neighbors': [3, 5, 11, 19],
                  'model__weights': ['uniform', 'distance'],
                  'model__n_jobs': [-1]
                  }
    classifier_knn = GridSearchCV(knn_pipe, parameters, iid=False, cv=3)
    classifier_knn = classifier_knn.fit(x_train, y_train.ravel())

    y_pred_knn_test = classifier_knn.predict(x_test)
    accuracy_knn_test = accuracy_score(y_test, y_pred_knn_test)
    print("Test set: ", accuracy_knn_test)

    sns.heatmap(confusion_matrix(y_test, y_pred_knn_test), annot=True, cmap='viridis', fmt='.0f')
    plt.show()


df = pd.read_csv('csv_files/glass.csv')
features = df.columns[:-1].tolist()
# df = data_preparation(df)

# Define X as features and y as labels
X = df[features]
y = df['Type']
seed = 42
test_size = 0.25
x_train, x_test, y_train, y_test = train_test_split(X, y, test_size=test_size, random_state=seed)
knn_classifier(df, x_train, x_test, y_train, y_test)
