# Импортируем библиотеки
from scipy.cluster.hierarchy import linkage, dendrogram
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
import umap
from sklearn.decomposition import PCA
from sklearn.manifold import TSNE
from scipy.stats import zscore
from sklearn.cluster import AgglomerativeClustering, KMeans


def hierarchy_clustering():
    # Создаем датафрейм
    glass_df = pd.read_csv("csv_files/glass.csv")
    # Исключаем информацию о странах
    types = list(glass_df.pop('Type'))
    glass_df.apply(zscore)

    # Извлекаем измерения как массив NumPy
    samples = glass_df.values

    # Реализация иерархической кластеризации при помощи функции linkage
    mergings = linkage(samples, method='ward')

    # Строим дендрограмму, указав параметры удобные для отображения
    dendrogram(mergings, labels=types, leaf_rotation=90)
    plt.title('Dendrogram')
    plt.xlabel('Data Points')
    plt.ylabel('Euclidean Distances')

    plt.show()


def show_distribution(glass_df):
    features = glass_df.columns[:-1].tolist()
    for feat in features:
        skew = glass_df[feat].skew()
        sns.distplot(glass_df[feat], kde=False, label='Skew = %.3f' % (skew), bins=30)
        plt.legend(loc='best')
        plt.show()

def get_pairplot(glass_df):
    umapped = umap.UMAP(n_components=4, random_state=200).fit_transform(glass_df)
    sns_plot = sns.pairplot(data=pd.DataFrame(umapped))
    sns_plot.savefig('graphs/pairplot4.png')


def draw_3d(x, y, z, myclusters):
    fig = plt.figure()
    ax = fig.gca(projection='3d')

    ax.scatter3D(x, y, z, c=myclusters)
    plt.show()


def get_umap(glass_df, myclusters):
    #scaler = StandardScaler()
    #data = scaler.fit_transform(glass_df)
    #umapped = umap.UMAP(n_components=3, n_neighbors=100, random_state=200).fit_transform(glass_df)
    umapped = umap.UMAP(n_components=3, random_state=200).fit_transform(glass_df)

    x = umapped[:, 0]
    y = umapped[:, 1]
    z = umapped[:, 2]
    draw_3d(x, y, z, myclusters)


def k_means_elbow(glass_df):
    cluster_no = range(1, 15)
    wcss = []
    for no in cluster_no:
        km = KMeans(no, random_state=1)
        km.fit(glass_df)
        wcss.append(km.inertia_)

    plt.figure(figsize=(16, 8))
    plt.plot(cluster_no, wcss, marker='o')
    plt.savefig("graphs/elbow.png")


def get_pca(glass_df, myclasters):
    pca = PCA(n_components=2)
    pca_2d = pca.fit_transform(glass_df)
    x = pca_2d[:, 0]
    y = pca_2d[:, 1]
    plt.scatter(x, y, c=myclasters)
    plt.show()


def pca_analysis(glass_df):
    pca = PCA(n_components=9)
    pca.fit_transform(glass_df)
    variance_ratio = pca.explained_variance_ratio_.tolist()

    print("Principal components analisis:")
    for i, pc in enumerate(variance_ratio):
        print("PC" + str(i + 1) + ": ", pc)


def k_means(new_glass_df, glass_df):
    km = KMeans(n_clusters=6, random_state=100)
    km.fit(new_glass_df.values)
    new_glass_df['Class'] = km.labels_
    print(new_glass_df['Class'].value_counts())

    new_myclusters = new_glass_df['Class']
    new_glass_df.pop('Class')
    get_umap(new_glass_df, new_myclusters)

    myclusters = glass_df['Type']
    glass_df.pop('Type')
    get_umap(glass_df, myclusters)


# reading data
glass_df = pd.read_csv("csv_files/glass.csv")
# glass_df = glass_df.apply(zscore)
# show_distribution(glass_df)

# checking the number of classes in the actual data
print(glass_df['Type'].value_counts())

# removing type column
new_glass_df = glass_df.drop('Type', axis=1)

pca_analysis(new_glass_df)
get_pca(new_glass_df, glass_df['Type'])
k_means(new_glass_df, glass_df)


