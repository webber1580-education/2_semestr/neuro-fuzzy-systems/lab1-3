# Lab 1-3

1) Source glass dataset - https://www.kaggle.com/uciml/glass
2) Source pulsar-star dataset - https://www.kaggle.com/pavanraj159/predicting-a-pulsar-star
3) prepare_data.py reduces initial dataset size to 1000 elements
4) lab13.py contains clusterization and data visualization
5) lab2.py contains classification