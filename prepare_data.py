#!/usr/bin/env python
import random

k = 1000
filename = 'pulsar_stars.csv'
with open(filename) as file:
    lines = file.read().splitlines()

if len(lines) > k:
    random_lines = random.sample(lines, k)
    print("\n".join(random_lines)) # print random lines

    with open("data.csv", 'w') as output_file:
        output_file.writelines(line + "\n" for line in random_lines)

elif lines: # file is too small
    print("\n".join(lines)) # print all lines
    with open(filename, 'wb', 0): # empty the file
        pass